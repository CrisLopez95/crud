package com.javarevolutions.demo.entity;

public class Empleados {

	private String nombre;
	private int numEmpleado;
	private int sueldo;
	private String empresa;
	
	public Empleados() {
		
	}

	public Empleados(String nombre, int numEmpleado, int sueldo, String empresa) {
		
		this.nombre = nombre;
		this.numEmpleado = numEmpleado;
		this.sueldo = sueldo;
		this.empresa = empresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumEmpleado() {
		return numEmpleado;
	}

	public void setNumEmpleado(int numEmpleado) {
		this.numEmpleado = numEmpleado;
	}

	public int getSueldo() {
		return sueldo;
	}

	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
		
	
}

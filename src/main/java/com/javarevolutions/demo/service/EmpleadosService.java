package com.javarevolutions.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.javarevolutions.demo.entity.Empleados;

@Service
public class EmpleadosService {


	//Listado Empleados
	
	private List<Empleados> empleado;
	
	public EmpleadosService() {
		empleado = new ArrayList<>();
		empleado.add(new Empleados("Veronica", 001, 20000, "BTS"));
		empleado.add(new Empleados("Daniel", 002, 12000, "QUALTOP"));
		empleado.add(new Empleados("Maria", 003, 17000, "CONTPAQ"));
		empleado.add(new Empleados("Juan", 004, 15000, "BTS"));
	}
	
	public List<Empleados> list(){
		return empleado;
	}
	
	//BuscarEmpleado
	
	public Empleados find(int numEmpleado) {
		for(Empleados empleado : empleado) {
			if(empleado.getNumEmpleado()== numEmpleado) {
				return empleado;
			}
		}
		return null;
	}
	
	//CrearEmpleado
	
	public Empleados save(Empleados emp) {
		empleado.add(emp);
		return emp;
	}
	
	//ActualizarEmpleado
	
	public Empleados update(int numEmpleado, Empleados emp) {
		int index = 0;
		for(Empleados l: empleado) {
			if(l.getNumEmpleado() == numEmpleado) {
				emp.setNumEmpleado(numEmpleado);
				empleado.set(index, emp);
			}
		}
	return emp;
	}
	
	//EliminarEmpleado
	
	public boolean delete(int numEmpleado) {
		for(Empleados e: empleado) {
			if(e.getNumEmpleado() == numEmpleado) {
				return empleado.remove(e);
			}
		}
		return false;
	}
}

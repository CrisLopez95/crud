package com.javarevolutions.demo.controller;



import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javarevolutions.demo.entity.Empleados;
import com.javarevolutions.demo.service.EmpleadosService;

@RestController
@RequestMapping("/empleados")
public class EmpleadosController {
	
	private final EmpleadosService service;
	
	public EmpleadosController(EmpleadosService service) {
		this.service = service;
	}
	
	@GetMapping
	public Iterable<Empleados> list(){
		return service.list();
 }

	@GetMapping("/{numEmpleado}")
	public Empleados find (@PathVariable("numEmpleado") int numEmpleado) {
		return service.find(numEmpleado);
	}
	
	@PostMapping
	public Empleados create(@RequestBody Empleados empleado) {
		return service.save(empleado);
	}
	
	@PutMapping("/{numEmpleado}")
	public Empleados update(@PathVariable("numEmpleado") int numEmpleado, @RequestBody Empleados empleado) {
		return service.update(numEmpleado, empleado);
	}
	
	@DeleteMapping("/{numEmpleado}")
	public boolean delete(@PathVariable("numEmpleado") int numEmpleado) {
		return service.delete(numEmpleado);
	}
}
